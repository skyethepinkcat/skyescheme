use std::{fs::File, io::Read, env, process};
use json::{self, JsonValue};
use levenshtein::levenshtein;

fn match_keys(search :&String, json_tree :&JsonValue) -> Result<String, &'static str> {

    for (k, _) in json_tree.entries() {
        if (levenshtein(search, k) <= 3) {
            return Ok(k.to_owned());
        }
    }
    
    return Err("Unknown Theme...");
}

fn main() -> Result<(), ()> {

    if env::args().len() < 2 {
        return Ok(());
    }

    let fileloc = env::var("HOME").unwrap() + "/.colors.json";
    let theme_name = match env::var("SKYE_THEME") {
        Ok(t) => t,
        Err(_) => {
            eprintln!("There doesn't seem to be a SKYE_THEME environment variable, defaulting to dracula", );
            "dracula".to_string()
        }

    };
    let mut file = File::open(&fileloc).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    let parsed = match json::parse(&contents)  {
        Ok(p) => p,
        Err(_) => {
            eprintln!("There doesn't seem to be a valid json file at {}...", &fileloc);
            process::exit(1);
        }
    };

    let theme = match_keys(&theme_name, &parsed).unwrap();

    let get = &(env::args().collect::<Vec<String>>())[1];

    let color = &parsed[&theme]["scheme"][get];
    if color.is_null() {
        eprintln!("{} is not a valid scheme color", &get);
        process::exit(1);
        
    }
    println!("{}", &parsed[&theme]["colors"][color.as_str().unwrap()].as_str().unwrap());

    return Ok(());
}
